abstract class ItemDownloadSuccessfulNotification {
  Future<void> notify();
}

abstract class LogSaveSuccessfulNotification {
  Future<void> notify();
}
