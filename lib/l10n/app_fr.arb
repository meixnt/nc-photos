{
	"appTitle": "Photos",
	"translator": "mgreil",
	"@translator": {
		"description": "Name of the translator(s) for this language"
	},
	"photosTabLabel": "Photos",
	"@photosTabLabel": {
		"description": "Label of the tab that lists user photos"
	},
	"albumsTabLabel": "Albums",
	"@albumsTabLabel": {
		"description": "Label of the tab that lists user albums"
	},
	"zoomTooltip": "Zoom",
	"@zoomTooltip": {
		"description": "Tooltip of the zoom button"
	},
	"refreshMenuLabel": "Rafraîchir",
	"@refreshMenuLabel": {
		"description": "Label of the refresh entry in menu"
	},
	"settingsMenuLabel": "Paramètres",
	"@settingsMenuLabel": {
		"description": "Label of the settings entry in menu"
	},
	"selectionAppBarTitle": "{count} sélectionné",
	"@selectionAppBarTitle": {
		"description": "Title of the contextual app bar that shows number of selected items",
		"placeholders": {
			"count": {
				"example": "1"
			}
		}
	},
	"addSelectedToAlbumSuccessNotification": "Tous les éléments ont été ajoutés à {album} avec succès",
	"@addSelectedToAlbumSuccessNotification": {
		"description": "Inform user that the selected items are added to an album successfully",
		"placeholders": {
			"album": {
				"example": "Sunday Walk"
			}
		}
	},
	"addSelectedToAlbumFailureNotification": "Échec de l'ajout d'éléments à l'album",
	"@addSelectedToAlbumFailureNotification": {
		"description": "Inform user that the selected items cannot be added to an album"
	},
	"addToAlbumTooltip": "Ajouter à l'album",
	"@addToAlbumTooltip": {
		"description": "Add selected items to an album"
	},
	"addToAlbumSuccessNotification": "Ajouté à {album} avec succès",
	"@addToAlbumSuccessNotification": {
		"description": "Inform user that the item is added to an album successfully",
		"placeholders": {
			"album": {
				"example": "Sunday Walk"
			}
		}
	},
	"addToAlbumFailureNotification": "Échec de l'ajout à l'album",
	"@addToAlbumFailureNotification": {
		"description": "Inform user that the item cannot be added to an album"
	},
	"addToAlbumAlreadyAddedNotification": "Élément déjà dans l'album",
	"@addToAlbumAlreadyAddedNotification": {
		"description": "Inform user that the item is already in the album"
	},
	"deleteSelectedProcessingNotification": "{count, plural, =1{Suppression de 1 élément} other{Suppression de {count} éléments}}",
	"@deleteSelectedProcessingNotification": {
		"description": "Inform user that the selected items are being deleted",
		"placeholders": {
			"count": {
				"example": "1"
			}
		}
	},
	"deleteSelectedSuccessNotification": "Tous les éléments ont été supprimés avec succès",
	"@deleteSelectedSuccessNotification": {
		"description": "Inform user that the selected items are deleted successfully"
	},
	"deleteSelectedFailureNotification": "{count, plural, =1{Échec de la suppression de 1 élément} other{Échec de la suppression de {count} éléments}}",
	"@deleteSelectedFailureNotification": {
		"description": "Inform user that some/all the selected items cannot be deleted",
		"placeholders": {
			"count": {
				"example": "1"
			}
		}
	},
	"archiveTooltip": "Archiver la selection",
	"@archiveTooltip": {
		"description": "Archive selected items"
	},
	"archiveSelectedProcessingNotification": "{count, plural, =1{Archivage 1 élément} other{Archivage de {count} éléments}}",
	"@archiveSelectedProcessingNotification": {
		"description": "Archiving the selected items",
		"placeholders": {
			"count": {
				"example": "1"
			}
		}
	},
	"archiveSelectedSuccessNotification": "Tous les éléments ont été archivés avec succès",
	"@archiveSelectedSuccessNotification": {
		"description": "Archived all selected items successfully"
	},
	"archiveSelectedFailureNotification": "{count, plural, =1{Échec de l'archivage de 1 élément} other{Échec de l'archivage de {count} éléments}}",
	"@archiveSelectedFailureNotification": {
		"description": "Cannot archive some of the selected items",
		"placeholders": {
			"count": {
				"example": "1"
			}
		}
	},
	"unarchiveTooltip": "Désarchiver la sélection",
	"@unarchiveTooltip": {
		"description": "Unarchive selected items"
	},
	"unarchiveSelectedProcessingNotification": "{count, plural, =1{Désarchivage de 1 élément} other{Désarchivage de {count} éléments}}",
	"@unarchiveSelectedProcessingNotification": {
		"description": "Unarchiving selected items",
		"placeholders": {
			"count": {
				"example": "1"
			}
		}
	},
	"unarchiveSelectedSuccessNotification": "Tous les éléments ont été désarchivés avec succès",
	"@unarchiveSelectedSuccessNotification": {
		"description": "Unarchived all selected items successfully"
	},
	"unarchiveSelectedFailureNotification": "{count, plural, =1{Échec du désarchivage de 1 élément} other{Échec du désarchivage de {count} éléments}}",
	"@unarchiveSelectedFailureNotification": {
		"description": "Cannot unarchive some of the selected items",
		"placeholders": {
			"count": {
				"example": "1"
			}
		}
	},
	"deleteTooltip": "Supprimer",
	"@deleteTooltip": {
		"description": "Delete selected items"
	},
	"deleteProcessingNotification": "Suppression de l'élément",
	"@deleteProcessingNotification": {
		"description": "Inform user that the item is being deleted"
	},
	"deleteSuccessNotification": "Élément supprimé avec succès",
	"@deleteSuccessNotification": {
		"description": "Inform user that the item is deleted successfully"
	},
	"deleteFailureNotification": "Échec de la suppression de l'élément",
	"@deleteFailureNotification": {
		"description": "Inform user that the item cannot be deleted"
	},
	"removeSelectedFromAlbumTooltip": "Supprimer la sélection de l'album",
	"@removeSelectedFromAlbumTooltip": {
		"description": "Tooltip of the button that remove selected items from an album"
	},
	"removeSelectedFromAlbumSuccessNotification": "{count, plural, =1{1 élément supprimé de l'album} other{{count} éléments supprimés de l'album}}",
	"@removeSelectedFromAlbumSuccessNotification": {
		"description": "Inform user that the selected items are removed from an album successfully",
		"placeholders": {
			"count": {
				"example": "1"
			}
		}
	},
	"removeSelectedFromAlbumFailureNotification": "Échec de la suppression des éléments de l'album",
	"@removeSelectedFromAlbumFailureNotification": {
		"description": "Inform user that the selected items cannot be removed from an album"
	},
	"addServerTooltip": "Ajouter un serveur",
	"@addServerTooltip": {
		"description": "Tooltip of the button that adds a new server"
	},
	"removeServerSuccessNotification": "{server} supprimé avec succès",
	"@removeServerSuccessNotification": {
		"description": "Inform user that a server is removed successfully",
		"placeholders": {
			"server": {
				"example": "http://www.example.com"
			}
		}
	},
	"createAlbumTooltip": "Créer un nouvel album",
	"@createAlbumTooltip": {
		"description": "Tooltip of the button that creates a new album"
	},
	"createAlbumFailureNotification": "Échec de la création de l'album",
	"@createAlbumFailureNotification": {
		"description": "Inform user that an album cannot be created"
	},
	"albumSize": "{count, plural, =0{Empty} =1{1 élément} other{{count} éléments}}",
	"@albumSize": {
		"description": "Number of items inside an album",
		"placeholders": {
			"count": {
				"example": "1"
			}
		}
	},
	"albumArchiveLabel": "Archive",
	"@albumArchiveLabel": {
		"description": "Archive"
	},
	"connectingToServer": "Connexion à\n{server}",
	"@connectingToServer": {
		"description": "Inform user that the app is connecting to a server",
		"placeholders": {
			"server": {
				"example": "http://www.example.com"
			}
		}
	},
	"nameInputHint": "Nom",
	"@nameInputHint": {
		"description": "Hint of the text field expecting name data"
	},
	"albumNameInputInvalidEmpty": "Veuillez saisir le nom de l'album",
	"@albumNameInputInvalidEmpty": {
		"description": "Inform user that the album name input field cannot be empty"
	},
	"skipButtonLabel": "IGNORER",
	"@skipButtonLabel": {
		"description": "Label of the skip button"
	},
	"confirmButtonLabel": "CONFIRMER",
	"@confirmButtonLabel": {
		"description": "Label of the confirm button"
	},
	"signInHeaderText": "Connectez-vous à un serveur Nextcloud",
	"@signInHeaderText": {
		"description": "Inform user what to do in sign in widget"
	},
	"signIn2faHintText": "*Utilisez un mot de passe d'application si vous avez activé l'authentification à deux facteurs sur le serveur",
	"@signIn2faHintText": {
		"description": "Notify users with 2FA enabled what should be done in order to sign in correctly"
	},
	"serverAddressInputHint": "Adresse du serveur",
	"@serverAddressInputHint": {
		"description": "Hint of the text field expecting server address data"
	},
	"serverAddressInputInvalidEmpty": "Veuillez saisir l'adresse du serveur",
	"@serverAddressInputInvalidEmpty": {
		"description": "Inform user that the server address input field cannot be empty"
	},
	"usernameInputHint": "Nom d'utilisateur",
	"@usernameInputHint": {
		"description": "Hint of the text field expecting username data"
	},
	"usernameInputInvalidEmpty": "Veuillez entrer votre nom d'utilisateur",
	"@usernameInputInvalidEmpty": {
		"description": "Inform user that the username input field cannot be empty"
	},
	"passwordInputHint": "Mot de passe",
	"@passwordInputHint": {
		"description": "Hint of the text field expecting password data"
	},
	"passwordInputInvalidEmpty": "Veuillez entrer votre mot de passe",
	"@passwordInputInvalidEmpty": {
		"description": "Inform user that the password input field cannot be empty"
	},
	"rootPickerHeaderText": "Choisissez les dossiers à inclure",
	"@rootPickerHeaderText": {
		"description": "Inform user what to do in root picker widget"
	},
	"rootPickerSubHeaderText": "Seules les photos à l'intérieur des dossiers seront affichées. Appuyez sur Ignorer pour tout inclure",
	"@rootPickerSubHeaderText": {
		"description": "Inform user what to do in root picker widget"
	},
	"rootPickerNavigateUpItemText": "(retourner en arrière)",
	"@rootPickerNavigateUpItemText": {
		"description": "Text of the list item to navigate up the directory tree"
	},
	"rootPickerUnpickFailureNotification": "Échec de la séparation de l'élément",
	"@rootPickerUnpickFailureNotification": {
		"description": "Failed while unpicking an item in the root picker list"
	},
	"rootPickerListEmptyNotification": "Veuillez choisir au moins un dossier ou appuyez sur ignorer pour tout inclure",
	"@rootPickerListEmptyNotification": {
		"description": "Error when user pressing confirm without picking any folders"
	},
	"setupWidgetTitle": "Commencer",
	"@setupWidgetTitle": {
		"description": "Title of the introductory widget"
	},
	"setupSettingsModifyLaterHint": "Vous pouvez modifier cela plus tard dans les paramètres",
	"@setupSettingsModifyLaterHint": {
		"description": "Inform user that they can modify this setting after the setup process"
	},
	"setupHiddenPrefDirNoticeDetail": "Cette application crée un dossier sur le serveur Nextcloud pour stocker les fichiers de préférences. Veuillez ne pas le modifier ou le supprimer à moins que vous ne prévoyiez de supprimer cette application",
	"@setupHiddenPrefDirNoticeDetail": {
		"description": "Inform user about the preference folder created by this app"
	},
	"settingsWidgetTitle": "Paramètres",
	"@settingsWidgetTitle": {
		"description": "Title of the Settings widget"
	},
	"settingsLanguageTitle": "Langue",
	"@settingsLanguageTitle": {
		"description": "Set display language"
	},
	"settingsExifSupportTitle": "Prise en charge EXIF",
	"@settingsExifSupportTitle": {
		"description": "Title of the EXIF support setting"
	},
	"settingsExifSupportTrueSubtitle": "Exiger une utilisation supplémentaire du réseau",
	"@settingsExifSupportTrueSubtitle": {
		"description": "Subtitle of the EXIF support setting when the value is true. The goal is to warn user about the possible side effects of enabling this setting"
	},
	"settingsAboutSectionTitle": "À propos",
	"@settingsAboutSectionTitle": {
		"description": "Title of the about section in settings widget"
	},
	"settingsVersionTitle": "Version",
	"@settingsVersionTitle": {
		"description": "Title of the version data item"
	},
	"settingsSourceCodeTitle": "Code source",
	"@settingsSourceCodeTitle": {
		"description": "Title of the source code item"
	},
	"settingsBugReportTitle": "Signaler un problème",
	"@settingsBugReportTitle": {
		"description": "Report issue"
	},
	"settingsTranslatorTitle": "Traducteur",
	"@settingsTranslatorTitle": {
		"description": "Title of the translator item"
	},
	"writePreferenceFailureNotification": "Échec du réglage des préférences",
	"@writePreferenceFailureNotification": {
		"description": "Inform user that the preference file cannot be modified"
	},
	"enableButtonLabel": "ACTIVER",
	"@enableButtonLabel": {
		"description": "Label of the enable button"
	},
	"exifSupportDetails": "L'activation de la prise en charge EXIF ​​rendra diverses métadonnées disponibles, telles que la date de prise de vue, le modèle de l'appareil photo, etc. Afin de lire ces métadonnées, une utilisation supplémentaire du réseau est requise pour télécharger l'image originale en taille réelle. L'application ne commencera à télécharger qu'une fois connectée à un réseau Wi-Fi",
	"@exifSupportDetails": {
		"description": "Detailed description of the exif support feature"
	},
	"exifSupportConfirmationDialogTitle": "Activer la prise en charge EXIF ?",
	"@exifSupportConfirmationDialogTitle": {
		"description": "Title of the dialog to confirm enabling exif support"
	},
	"doneButtonLabel": "TERMINÉ",
	"@doneButtonLabel": {
		"description": "Label of the done button"
	},
	"nextButtonLabel": "SUIVANT",
	"@nextButtonLabel": {
		"description": "Label of the next button"
	},
	"connectButtonLabel": "CONNECTER",
	"@connectButtonLabel": {
		"description": "Label of the connect button"
	},
	"rootPickerSkipConfirmationDialogContent": "Tous vos fichiers seront inclus",
	"@rootPickerSkipConfirmationDialogContent": {
		"description": "Inform user what happens after skipping root picker"
	},
	"megapixelCount": "{count}MP",
	"@megapixelCount": {
		"description": "Resolution of an image in megapixel",
		"placeholders": {
			"count": {
				"example": "1.3"
			}
		}
	},
	"secondCountSymbol": "{count}s",
	"@secondCountSymbol": {
		"description": "Number of seconds",
		"placeholders": {
			"count": {
				"example": "1"
			}
		}
	},
	"millimeterCountSymbol": "{count}mm",
	"@millimeterCountSymbol": {
		"description": "Number of millimeters",
		"placeholders": {
			"count": {
				"example": "1"
			}
		}
	},
	"detailsTooltip": "Détails",
	"@detailsTooltip": {
		"description": "Tooltip of the details button"
	},
	"downloadTooltip": "Télécharger",
	"@downloadTooltip": {
		"description": "Tooltip of the download button"
	},
	"downloadProcessingNotification": "Téléchargement du fichier",
	"@downloadProcessingNotification": {
		"description": "Inform user that the file is being downloaded"
	},
	"downloadSuccessNotification": "Fichier téléchargé avec succès",
	"@downloadSuccessNotification": {
		"description": "Inform user that the file is downloaded successfully"
	},
	"downloadFailureNotification": "Échec du téléchargement du fichier",
	"@downloadFailureNotification": {
		"description": "Inform user that the file cannot be downloaded"
	},
	"downloadFailureNoPermissionNotification": "Exiger une autorisation d'accès au stockage",
	"@downloadFailureNoPermissionNotification": {
		"description": "Inform user that the file cannot be downloaded due to missing storage permission"
	},
	"nextTooltip": "Suivant",
	"@nextTooltip": {
		"description": "Tooltip of the next button"
	},
	"previousTooltip": "Précédent",
	"@previousTooltip": {
		"description": "Tooltip of the previous button"
	},
	"webSelectRangeNotification": "Maintenez la touche Maj enfoncée + cliquez pour tout sélectionner entre les deux",
	"@webSelectRangeNotification": {
		"description": "Inform web user how to select items in range"
	},
	"mobileSelectRangeNotification": "Appuyez longuement sur un autre élément pour tout sélectionner entre",
	"@mobileSelectRangeNotification": {
		"description": "Inform mobile user how to select items in range"
	},
	"updateDateTimeDialogTitle": "Modifier la date et l'heure",
	"@updateDateTimeDialogTitle": {
		"description": "Dialog to modify the date & time of a file"
	},
	"dateSubtitle": "Date",
	"timeSubtitle": "Heure",
	"dateYearInputHint": "Année",
	"dateMonthInputHint": "Mois",
	"dateDayInputHint": "Jour",
	"timeHourInputHint": "Heure",
	"timeMinuteInputHint": "Minute",
	"dateTimeInputInvalid": "Valeur invalide",
	"@dateTimeInputInvalid": {
		"description": "Invalid date/time input (e.g., non-numeric characters)"
	},
	"updateDateTimeFailureNotification": "Échec de la modification de la date et de l'heure",
	"@updateDateTimeFailureNotification": {
		"description": "Failed to set the date & time of a file"
	},
	"albumDirPickerHeaderText": "Choisissez les dossiers à associer",
	"@albumDirPickerHeaderText": {
		"description": "Pick the folders for a folder based album"
	},
	"albumDirPickerSubHeaderText": "Seules les photos des dossiers associés seront incluses dans cet album",
	"@albumDirPickerSubHeaderText": {
		"description": "Pick the folders for a folder based album"
	},
	"albumDirPickerListEmptyNotification": "Veuillez choisir au moins un dossier",
	"@albumDirPickerListEmptyNotification": {
		"description": "Error when user pressing confirm without picking any folders"
	},
	"createAlbumDialogBasicLabel": "Simple",
	"@createAlbumDialogBasicLabel": {
		"description": "Simple album"
	},
	"createAlbumDialogBasicDescription": "Album simple organise les photos quelle que soit la hiérarchie des fichiers sur le serveur",
	"@createAlbumDialogBasicDescription": {
		"description": "Describe what a simple album is"
	},
	"createAlbumDialogFolderBasedLabel": "Basé sur le dossier",
	"@createAlbumDialogFolderBasedLabel": {
		"description": "Folder based album"
	},
	"createAlbumDialogFolderBasedDescription": "L'album basé sur un dossier reflète le contenu d'un dossier",
	"@createAlbumDialogFolderBasedDescription": {
		"description": "Describe what a folder based album is"
	},
	"convertBasicAlbumMenuLabel": "Convertir en album simple",
	"@convertBasicAlbumMenuLabel": {
		"description": "Convert a non-simple album to a simple one"
	},
	"convertBasicAlbumConfirmationDialogTitle": "Convertir en album simple",
	"@convertBasicAlbumConfirmationDialogTitle": {
		"description": "Make sure the user wants to perform the conversion"
	},
	"convertBasicAlbumConfirmationDialogContent": "Le contenu de cet album ne sera plus géré par l'application. Vous pourrez librement ajouter ou supprimer des photos.\n\nCette conversion est irréversible",
	"@convertBasicAlbumConfirmationDialogContent": {
		"description": "Make sure the user wants to perform the conversion"
	},
	"convertBasicAlbumSuccessNotification": "Album converti avec succès",
	"@convertBasicAlbumSuccessNotification": {
		"description": "Successfully converted the album"
	},
	"importFoldersTooltip": "Importer des dossiers",
	"@importFoldersTooltip": {
		"description": "Menu entry in the album page to import folders as albums"
	},
	"albumImporterHeaderText": "Importer des dossiers sous forme d'albums",
	"@albumImporterHeaderText": {
		"description": "Import folders as albums"
	},
	"albumImporterSubHeaderText": "Les dossiers suggérés sont répertoriés ci-dessous. Selon le nombre de fichiers sur votre serveur, cela peut prendre un certain temps",
	"@albumImporterSubHeaderText": {
		"description": "Import folders as albums"
	},
	"importButtonLabel": "IMPORTER",
	"albumImporterProgressText": "Importation de dossiers",
	"@albumImporterProgressText": {
		"description": "Message shown while importing"
	},
	"editAlbumMenuLabel": "Modifier l'album",
	"@editAlbumMenuLabel": {
		"description": "Edit the opened album"
	},
	"doneButtonTooltip": "Terminé",
	"editTooltip": "Modifier",
	"editAccountConflictFailureNotification": "Un compte existe déjà avec les mêmes paramètres",
	"@editAccountConflictFailureNotification": {
		"description": "Error when user modified an account such that it's identical to another one"
	},
	"genericProcessingDialogContent": "S'il vous plaît, attendez",
	"@genericProcessingDialogContent": {
		"description": "Generic dialog shown when the app is temporarily blocking user input to work on something"
	},
	"sortTooltip": "Trier",
	"sortOptionDialogTitle": "Trier par",
	"@sortOptionDialogTitle": {
		"description": "Select how the photos should be sorted"
	},
	"sortOptionTimeAscendingLabel": "Le plus âgé en premier",
	"@sortOptionTimeAscendingLabel": {
		"description": "Sort by time, in ascending order"
	},
	"sortOptionTimeDescendingLabel": "Le plus récent d'abord",
	"@sortOptionTimeDescendingLabel": {
		"description": "Sort by time, in descending order"
	},
	"albumEditDragRearrangeNotification": "Appuyez longuement et faites glisser un élément pour le réorganiser manuellement",
	"@albumEditDragRearrangeNotification": {
		"description": "Instructions on how to rearrange photos"
	},
	"albumAddTextTooltip": "Ajouter du texte",
	"@albumAddTextTooltip": {
		"description": "Add some text that display between photos to an album"
	},
	"shareTooltip": "Partager",
	"@shareTooltip": {
		"description": "Share selected items to other apps"
	},
	"shareSelectedEmptyNotification": "Sélectionnez quelques photos à partager",
	"@shareSelectedEmptyNotification": {
		"description": "Shown when user pressed the share button with only non-sharable items (e.g., text labels) selected"
	},
	"shareDownloadingDialogContent": "Téléchargement",
	"@shareDownloadingDialogContent": {
		"description": "Downloading photos to be shared"
	},
	"searchTooltip": "Chercher",
	"albumSearchTextFieldHint": "Rechercher des albums",
	"clearTooltip": "Nettoyer",
	"@clearTooltip": {
		"description": "Clear some sort of user input, typically a text field"
	},
	"listNoResultsText": "Aucun résultat",
	"@listNoResultsText": {
		"description": "When there's nothing in a list"
	},
	"listEmptyText": "Vide",
	"@listEmptyText": {
		"description": "When there's nothing in a list"
	},
	"albumTrashLabel": "Corbeille",
	"@albumTrashLabel": {
		"description": "Deleted photos"
	},
	"restoreTooltip": "Restaurer",
	"@restoreTooltip": {
		"description": "Restore selected items from trashbin"
	},
	"restoreSelectedProcessingNotification": "{count, plural, =1{Restauration d'1 élément} other{Restauration de {count} éléments}}",
	"@restoreSelectedProcessingNotification": {
		"description": "Restoring selected items from trashbin",
		"placeholders": {
			"count": {
				"example": "1"
			}
		}
	},
	"restoreSelectedSuccessNotification": "Tous les éléments ont été restaurés avec succès",
	"@restoreSelectedSuccessNotification": {
		"description": "Restored all selected items from trashbin successfully"
	},
	"restoreSelectedFailureNotification": "{count, plural, =1{Échec de la restauration de 1 élément} other{Échec de la restauration de {count} éléments}}",
	"@restoreSelectedFailureNotification": {
		"description": "Cannot restore some of the selected items from trashbin",
		"placeholders": {
			"count": {
				"example": "1"
			}
		}
	},
	"restoreProcessingNotification": "Restauration de l'élément",
	"@restoreProcessingNotification": {
		"description": "Restoring the opened item from trashbin"
	},
	"restoreSuccessNotification": "Élément restauré avec succès",
	"@restoreSuccessNotification": {
		"description": "Restored the opened item from trashbin successfully"
	},
	"restoreFailureNotification": "Échec de la restauration de l'élément",
	"@restoreFailureNotification": {
		"description": "Cannot restore the opened item from trashbin"
	},
	"deletePermanentlyTooltip": "Supprimer définitivement",
	"@deletePermanentlyTooltip": {
		"description": "Permanently delete selected items from trashbin"
	},
	"deletePermanentlyConfirmationDialogTitle": "Supprimer définitivement",
	"@deletePermanentlyConfirmationDialogTitle": {
		"description": "Make sure the user wants to delete the items"
	},
	"deletePermanentlyConfirmationDialogContent": "Les éléments sélectionnés seront supprimés définitivement du serveur.\n\nCette action est irréversible",
	"@deletePermanentlyConfirmationDialogContent": {
		"description": "Make sure the user wants to delete the items"
	},
	"albumSharedLabel": "Partagé",
	"@albumSharedLabel": {
		"description": "A small label placed next to a shared album"
	},
	"setAlbumCoverProcessingNotification": "Définir une photo comme couverture d'album",
	"@setAlbumCoverProcessingNotification": {
		"description": "Setting the opened item as the album cover"
	},
	"setAlbumCoverSuccessNotification": "Couverture changé avec succès",
	"@setAlbumCoverSuccessNotification": {
		"description": "Set the opened item as the album cover successfully"
	},
	"setAlbumCoverFailureNotification": "Échec du changement de couverture",
	"@setAlbumCoverFailureNotification": {
		"description": "Cannot set the opened item as the album cover"
	},
	"metadataTaskProcessingNotification": "Traitement des métadonnées de l'image en arrière-plan",
	"@metadataTaskProcessingNotification": {
		"description": "Shown when the app is reading image metadata"
	},
	"metadataTaskPauseNoWiFiNotification": "En attente du Wi-Fi",
	"@metadataTaskPauseNoWiFiNotification": {
		"description": "Shown when the app has paused reading image metadata"
	},
	"configButtonLabel": "CONFIGURER",
	"useAsAlbumCoverTooltip": "Utiliser comme couverture d'album",
	"changelogTitle": "Journal des modifications",
	"@changelogTitle": {
		"description": "Title of the changelog dialog"
	},
	"serverCertErrorDialogTitle": "Le certificat du serveur ne peut pas être approuvé",
	"@serverCertErrorDialogTitle": {
		"description": "Title of the dialog to warn user about an untrusted SSL certificate"
	},
	"serverCertErrorDialogContent": "Le serveur peut être piraté ou quelqu'un essaie de voler vos informations",
	"@serverCertErrorDialogContent": {
		"description": "Warn user about an untrusted SSL certificate"
	},
	"advancedButtonLabel": "AVANCÉ",
	"@advancedButtonLabel": {
		"description": "Label of the advanced button"
	},
	"whitelistCertDialogTitle": "Certificat inconnu de la liste blanche ?",
	"@whitelistCertDialogTitle": {
		"description": "Title of the dialog to let user decide whether to whitelist an untrusted SSL certificate"
	},
	"whitelistCertDialogContent": "Vous pouvez ajouter le certificat à la liste blanche pour que l'application l'accepte. AVERTISSEMENT : Cela pose un grand risque pour la sécurité. Assurez-vous que le certificat est auto-signé par vous ou une partie de confiance\n\nHôte : {hôte}\nEmpreinte digitale : {fingerprint}",
	"@whitelistCertDialogContent": {
		"description": "Let user decide whether to whitelist an untrusted SSL certificate",
		"placeholders": {
			"host": {
				"example": "www.example.com"
			},
			"fingerprint": {
				"example": "da39a3ee5e6b4b0d3255bfef95601890afd80709"
			}
		}
	},
	"whitelistCertButtonLabel": "ACCEPTER LE RISQUE ET AJOUTER À LA LISTE BLANCHE",
	"@whitelistCertButtonLabel": {
		"description": "Label of the whitelist certificate button"
	},
	"errorUnauthenticated": "Accès non authentifié. Veuillez vous reconnecter si le problème persiste",
	"@errorUnauthenticated": {
		"description": "Error message when server responds with HTTP401"
	},
	"errorDisconnected": "Impossible de se connecter. Le serveur peut être hors ligne ou votre appareil peut être déconnecté",
	"@errorDisconnected": {
		"description": "Error message when the app can't connect to the server"
	},
	"errorLocked": "Le fichier est verrouillé sur le serveur. Veuillez réessayer plus tard",
	"@errorLocked": {
		"description": "Error message when server responds with HTTP423"
	},
	"errorInvalidBaseUrl": "Impossible de communiquer. Veuillez vous assurer que l'adresse est l'URL de base de votre instance Nextcloud",
	"@errorInvalidBaseUrl": {
		"description": "Error message when the base URL is invalid"
	},
	"errorWrongPassword": "Impossible à authentifier. Veuillez vérifier le nom d'utilisateur et le mot de passe",
	"@errorWrongPassword": {
		"description": "Error message when the username or password is wrong"
	},
	"errorServerError": "Erreur du serveur. Veuillez vous assurer que le serveur est correctement configuré",
	"@errorServerError": {
		"description": "HTTP 500"
	}
}
